package transform

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

class TransformMap {

  abstract class Funcion // case class
  case class FuncionUnaria(f : Int => Int) extends Funcion
  case class FuncionBinaria(f : (Int,Int) => Int) extends Funcion

  def lambdaPot = FuncionUnaria((x:Int) => x*x)
  def lambdaSum = FuncionBinaria((x:Int,y:Int) => x+y)
  def sum4 = (x:Int) => x+4
}
object TransformMap extends App { // Companion object

  val transFormMap : TransformMap = new TransformMap

  val futPot : Future[Int] = Future {
    Thread sleep(1000)
    val pot = transFormMap.lambdaPot.f(3)
    pot
  }

  val futSum : Future[Int] = Future {
    Thread sleep(1000)
    val sum = transFormMap.lambdaSum.f(3,5)
    sum
  }

  val futMapSum4 = futSum.map(transFormMap sum4)
  futMapSum4 onComplete {
    case Success(r) => println("futMapSum4 resolution value is " + r)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  val futPotDiv = futPot.map(x => x/x)
  futPotDiv onComplete {
    case Success(r) => println("futPotSum resolution value is " + r)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  val futa = Future("a")
  // Future[String] => (String => String) => Future[String]
  val futA = futa.map(_.toUpperCase).onComplete(fut => println(fut.getOrElse("Error")))

  println("futMapSum4 is complete " + futMapSum4.isCompleted)
  println("futPotDiv is complete " + futPotDiv.isCompleted)
  Await.result(futMapSum4, Duration(100, "seconds"))
  Await.result(futPotDiv, Duration(100, "seconds"))
}
