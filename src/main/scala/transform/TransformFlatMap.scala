package transform

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object TransformFlatMap extends App {

  val futList: Future[List[Int]] = Future {Thread.sleep(2000); List.range(1,10)}

  def filtraPares(xs : List[Int]) : Future[List[Int]] = Future {
    println(xs)
    Thread.sleep(2000)
    val ys = xs.filter(x=> x%2==0)
    ys
  }

  val futFlatMap = futList flatMap {
    list => filtraPares(list)
  }
  futFlatMap onComplete {
    case Success(result) => println("futFlatMap value " + result)
    case Failure(t) => println("futFlatMap. An error has occured: " + t.getMessage)
  }

  while(!futFlatMap.isCompleted){} // Aberración
}
