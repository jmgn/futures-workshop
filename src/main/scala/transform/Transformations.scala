package transform

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Random, Success}

object Transformations extends App {
  val r : Random = new Random()

  // Ejemplo adaptado de : https://docs.scala-lang.org/overviews/core/futures.html#blocking
  val coinAQuote = Future { Thread.sleep(r.nextInt(1000)); r.nextInt(700) }
  val coinBQuote = Future { Thread.sleep(r.nextInt(1000)); r.nextInt(1000) }
  def isProfitable(a: Int, b: Int) = {
    println("Value A = " + a + " Value B = " + b)
    a < b
  }
  def buy(num: Int, quote: Int) = {
    val amount = num*quote
    println(s"Buy $quote, total amount " + amount)
    amount
  }
  val purchase = coinAQuote flatMap {  /* flatMap :: Future[Int] -> (Int -> Future[Int]) -> Future[Int] */
                 /*Future*/
    a =>
    /*Int*/
      coinBQuote
      /*Future*/
        .filter(b => isProfitable(a, b)) /*filter :: Future[Int] -> (Int -> Boolean) -> Future[Int]*/
              /*Int*/   /*Boolean*/
        .map(b => buy(2, b)) /* map :: Future[Int] -> (Int -> Int) -> Future[Int] */
           /*Int*/ /*Int*/
  }
  purchase onComplete {
    case Success(result) => println(result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  while(!purchase.isCompleted) {}
}
