package transform

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object TransformFor extends App {

  // In the expression for the futures do not run parallel
  val initfutForNoParallel = System.currentTimeMillis()

  val futForNoParallel = for {
    x <- Future {Thread.sleep(1000); 1+1}
    y <- Future {Thread.sleep(1000); 1+1}
  } yield x+y

  futForNoParallel onComplete {
    case Success(result) => println("futForNoParallel Time " + (System.currentTimeMillis() - initfutForNoParallel) + " Result " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }
  println("futForNoParallel is complete " + futForNoParallel.isCompleted)

  /**
    * Because the futures are declared out of for, the language can run then operation in parallel.
    * See the console, the time of execute this future is not the sum of the two futures
    */
  val initfutForParallel = System currentTimeMillis

  val futSumSleep = Future {Thread.sleep(1000); 2}
  val futSumSleep1 = Future {Thread.sleep(1000); 3}
  val futForParallel = for {
    x <- futSumSleep
    y <- futSumSleep1
  } yield x+y
  futForParallel onComplete {
    case Success(result) => println("futForParallel Time "
      + (System.currentTimeMillis() - initfutForParallel)
      + " Result " + result)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  while(!futForNoParallel.isCompleted
    || !futForParallel.isCompleted){}

  // Esto no es con futuros
  // Otro ejemplo del uso de los for-comprehension
  val listaTuplasFor = for {
    x <- List(1, 2, 3)
    y <- List(true, false)
  } yield (x, y)
  println(listaTuplasFor)

  // Es equivalente a hacer:
  val listaTuplasNoFor = List(1, 2, 3).flatMap(x =>
    List(true, false).map( y => (x, y))
  )
  println(listaTuplasNoFor)

  val lista3TuplaFor = for {
    x <- List(1, 2)
    y <- List(true, false)
    z <- List("a", "b")
  } yield (x, y, z)
  println(lista3TuplaFor)

  val lista3TuplaNoFor = List(1, 2).flatMap(x =>
    List(true, false).flatMap( y =>
      List("a", "b").map(z => (x, y, z))
    )
  )
  println(lista3TuplaNoFor)

}
