package intro

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object Presentation extends App { // Singlenton object

  val fut : Future[Int] = Future { // Tipado estático, inferencia de tipos
    Thread sleep 2000
    1 + 1 // No return
  }

  fut onComplete { // Pattern matching
    case Success(r) => println("fut resolution value is " + r)
    case Failure(r) => println("An error has occured: " + r.getMessage)
  }
  println("future is complete " + fut.isCompleted)

  Await.result(fut, Duration(100, "seconds")) // Blocking

  println("future is complete " + fut.isCompleted)
}
