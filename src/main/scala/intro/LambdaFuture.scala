package intro

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

case class LambdaFuture() { // case class
  def calculate(f : Int => Int, x : Int) = f(x) // HOF
}

object LambdaFuture extends App { // Companion objects
  val lamdaFuture = LambdaFuture()
  val futSum : Future[Int] = Future {
    Thread sleep 2000
    lamdaFuture.calculate((x:Int) => x + 3, 5) // Lambda
  }
  futSum onComplete {
    case Success(r) => println("futSum value is " + r)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }
  println("future is complete " + futSum.isCompleted)
  Await.result(futSum, Duration(100, "seconds"))
  println("future is complete " + futSum.isCompleted)
}
